import os
import re


def calls_input_or_include(line):
    """
    Determines whether or not a read in line contains an uncommented out
    \\input{} statement. Allows only spaces between start of line and
    '\\input{}'.
    """
    tex_input_regex = r"""(^[^\%]*\\input{[^}]*})|(^[^\%]*\\include{[^}]*})"""  # input or include
    return re.search(tex_input_regex, line)


def get_input_filename(line):
    """
    Gets the file name from a line containing an input statement.
    """
    tex_input_filename_regex = r"""{[^}]*"""
    m = re.search(tex_input_filename_regex, line)
    return m.group()[1:]


def combine_path(base_path, relative_ref):
    """
    Combines the base path of the tex document being worked on with the
    relate reference found in that document.
    """
    if base_path != "":
        os.chdir(base_path)
    # Handle if .tex is supplied directly with file name or not
    if relative_ref.endswith('.tex'):
        return os.path.join(base_path, relative_ref)
    else:
        return os.path.abspath(relative_ref) + '.tex'


def flatten_file(base_file_name, current_path):
    """
    Recursively-defined function that takes as input a file and returns it
    with all the inputs replaced with the contents of the referenced file.
    """
    flattened_content = []
    file = open(base_file_name, "r")
    for line in file:
        if calls_input_or_include(line):
            new_base_filename = combine_path(current_path, get_input_filename(line))
            flattened_content += flatten_file(new_base_filename, current_path)
            flattened_content.append('\n')  # add a new line after each file input
        else:
            flattened_content.append(line)
    file.close()
    return flattened_content


def main(base_filename, output_filename):
    """
    This "flattens" a LaTeX document by replacing all \\input{X} lines with the
    text actually contained in X.
    """
    current_path = os.path.split(base_filename)[0]
    file = open(output_filename, "w")
    file.write(''.join(flatten_file(base_filename, current_path)))
    file.close()
    return None


if __name__ == '__main__':
    main_file = input('What is your main tex-file? ')
    if not main_file.endswith('.tex'):
        main_file += '.tex'
    flat_file = input('What file do you want to save the flattened content to? ')
    if not flat_file.endswith('.tex'):
        flat_file += '.tex'
    main(main_file, flat_file)
