# Flatten LaTeX or flattex

This scripts flattens a collection of LaTeX files that make use of `\include` and `\input` commands.

## Requirements
You'll need python 3 and some latex files.

## Usage
In order to use the script, simply execute it in the directory with the latex files from the command line and follow the instructions:
`py flattex.py`

## Credits
The script is mostly copied from [John Joseph Horton](https://github.com/johnjosephhorton/flatex)